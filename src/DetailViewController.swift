import UIKit

class DetailViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var dateField: UIDatePicker!
    @IBOutlet weak var imageField: UIImageView!
    
    var listViewController:ListViewController? = nil
    let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
//        title = "Memento"
        if (listViewController?.selectedItem !== nil) {
            nameField.text = listViewController?.selectedItem?.value(forKey: "title") as? String
            dateField.date = (listViewController?.selectedItem?.value(forKey: "date") as? Date)!
            if let image = listViewController?.selectedItem?.value(forKey: "image") as? Data {
                imageField.image = UIImage(data: image)
            }
        }
    }
    
    @IBAction func onSave(_ sender: Any) {
        if (listViewController?.selectedItem !== nil) {
            listViewController?.updateItem(name: nameField.text!, dob: dateField.date, photo: imageField.image !== nil ? UIImageJPEGRepresentation(imageField.image!, 1)! : nil)
        } else {
            listViewController?.addItem(name: nameField.text!, dob: dateField.date, photo: imageField.image !== nil ? UIImageJPEGRepresentation(imageField.image!, 1)! : nil)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSelectImage(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func onClearImage(_ sender: Any) {
        imageField.image = nil
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as! UIImage
        imageField.image = image
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}


