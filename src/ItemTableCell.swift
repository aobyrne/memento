import UIKit
import CoreData

class ItemTableCell: UITableViewCell {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var days: UIButton!
    
    @IBAction func onTapAge(_ sender: Any) {
        toggleAge()
        item?.setValue(Utils.getModeInt(mode: ageMode), forKeyPath: "mode")
        try! context.save()
    }
    
    var ageMode:Utils.Modes = Utils.Modes.days {
        didSet {
            let dob:Date = item?.value(forKeyPath: "date") as! Date
            var title:String
            switch ageMode {
                case .days:
                    title = "\(Utils.getAgeDays(date: dob)) days"
                case .months:
                    title = "\(Utils.getAgeMonths(date: dob)) months"
                case .years:
                    title = "\(Utils.getAgeYears(date: dob)) years"
            }
            days?.setTitle(title, for: UIControlState.normal)
        }
    }
    
    var item:NSManagedObject? {
        didSet {
            if let image = item?.value(forKeyPath: "image") as? Data {
                photo?.image = UIImage(data: image)
            }
            
            name?.text = item?.value(forKeyPath: "title") as? String
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = DateFormatter.Style.medium
            let formattedDate = dateFormatter.string(from: item?.value(forKeyPath: "date") as! Date)
            date?.text = formattedDate
            
            if let mode = item?.value(forKeyPath: "mode") as? Int16 {
                ageMode = Utils.getMode(mode: mode)
            }
        }
    }
    
    func toggleAge() {
        switch ageMode {
            case .years:
                ageMode = Utils.Modes.months
            case .months:
                ageMode = Utils.Modes.days
            case .days:
                ageMode = Utils.Modes.years
        }
    }
    
//    override func willTransition(to state: UITableViewCellStateMask) {
//        super.willTransition(to: state)
//        self.date.isHidden = !self.isEditing
//        self.days.isHidden = !self.isEditing
//        DispatchQueue.main.async {
//            UIView.animate(withDuration: 0.1, animations: {
//                self.date.alpha = self.isEditing ? 0 : 1
//                self.days.alpha = self.isEditing ? 0 : 1
//            }, completion: {
//                _ in
//                self.date.isHidden = self.isEditing
//                self.days.isHidden = self.isEditing
//            })
//        }
//    }
    
}
