import UIKit
import CoreData

class ListViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    var items: [NSManagedObject] = []
    var selectedItem: NSManagedObject?
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        title = "Memento"
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if ((tableView.indexPathForSelectedRow) != nil) {
            tableView.deselectRow(at: tableView.indexPathForSelectedRow!, animated: true)
            self.selectedItem = nil
        }
        loadItems()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "detailSegue") {
            let vc: DetailViewController? = segue.destination as? DetailViewController
            vc?.listViewController = self
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.deleteItem(indexPath: indexPath)
        }
    }
    
    @IBAction func onEdit(_ sender: Any) {
        let isEditing = !tableView.isEditing
        tableView.setEditing(isEditing, animated: true)
        editButton.title = isEditing ? "Done" : "Edit"
    }
    
    @IBAction func onAdd(_ sender: Any) {
        self.selectedItem = nil
        performSegue(withIdentifier: "detailSegue", sender: self)
    }
    
    func loadItems() {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Item")
        let sortDescriptor = NSSortDescriptor(key: "index", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        items = try! context.fetch(fetchRequest)
    }
    
    func addItem(name: String, dob: Date, photo: Data? = nil) {
        let entity = NSEntityDescription.entity(forEntityName: "Item", in: context)!
        let item = NSManagedObject(entity: entity, insertInto: context)
        var index = 0
        if (items.count != 0) {
            index = items.index(of: items.last!)! + 1
        }
        item.setValue(index, forKeyPath: "index")
        item.setValue(name, forKeyPath: "title")
        item.setValue(dob, forKeyPath: "date")
        item.setValue(photo, forKeyPath: "image")
        items.append(item)
        let indexPath: IndexPath = IndexPath(row: index, section: 0)
        tableView.insertRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        try! context.save()
    }
    
    func updateItem(name: String, dob: Date, photo: Data? = nil) {
        selectedItem?.setValue(name, forKeyPath: "title")
        selectedItem?.setValue(dob, forKeyPath: "date")
        selectedItem?.setValue(photo, forKeyPath: "image")
        try! context.save()
        let indexPath:IndexPath = IndexPath(row: selectedItem?.value(forKey: "index") as! Int, section: 0)
        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
    }
    
    func deleteItem(indexPath: IndexPath) {
        let item: NSManagedObject? = items.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
        context.delete(item!)
        try! context.save()
    }

}

// MARK: - UITableViewDelegate
extension ListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = items[indexPath.row]
        self.selectedItem = item
        performSegue(withIdentifier: "detailSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let item = items[sourceIndexPath.row]
        items.remove(at: sourceIndexPath.row)
        items.insert(item, at: destinationIndexPath.row)
        var i = 0
        for obj in items {
            obj.setValue(i, forKey: "index")
            i = i + 1
        }
        try! context.save()
    }
    
}

// MARK: - UITableViewDataSource
extension ListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemTableCell", for: indexPath) as! ItemTableCell
        cell.item = item
        return cell
    }
    
}

