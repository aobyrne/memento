import Foundation

class Utils {
    
    static func getAgeDays(date: Date) -> Int {
        let components = NSCalendar.current.dateComponents([.day], from: date, to: Date())
        return components.day!
    }
    
    static func getAgeMonths(date: Date) -> Int {
        let components = NSCalendar.current.dateComponents([.month], from: date, to: Date())
        return components.month!
    }
    
    static func getAgeYears(date: Date) -> Float {
        var age:Float = Float(getAgeDays(date: date) - 1) / 365
        age = floor(age * 10) / 10 // 1 decimal place
        return age
    }
    
    enum Modes {
        case days, months, years
    }
    
    static func getMode(mode: Int16) -> Modes {
        switch mode {
            case 1:
                return Modes.days
            case 2:
                return Modes.months
            case 3:
                return Modes.years
            default:
                return Modes.years
        }
    }
    
    static func getModeInt(mode: Modes) -> Int16 {
        switch mode {
            case Modes.days:
                return 1
            case Modes.months:
                return 2
            case Modes.years:
                return 3
        }
    }
    
}
